function verify() {
	var password1 = document.forms['form']['password'].value;
	var password2 = document.forms['form']['verifyPassword'].value;
	if (password1 == null || password1 == "" || password1 != password2) {
		document.getElementById("error").innerHTML = "Please check your passwords";
		return false;
	}
}

function submitProject(){
	var projectNumber = document.forms['form']['project_num'].value;
	var projectName = document.forms['form']['project_name'].value;
	var projectName_length = projectName.length;	
	var budget = document.forms['form']['budget_cost'].value;
	
	var square_ft = document.forms['form']['square_ft'].value;
	
	
	if(isNaN(projectNumber)){
		document.getElementById("error").innerHTML = "Alphanumeric not allowed";
		return false;
	}
	
	if(isNaN(square_ft)){
		document.getElementById("error").innerHTML = "Alphanumeric not allowed";
		return false;
	}
	
	if(isNaN(budget)){
		document.getElementById("error").innerHTML = "Alphanumeric not allowed";
		return false;
	}
}

function myFunction() {
	  var input, filter, table, tr, td, i;
	  input = document.getElementById("myInput");
	  filter = input.value.toUpperCase();
	  table = document.getElementById("myTable");
	  tr = table.getElementsByTagName("tr");
	  for (i = 0; i < tr.length; i++) {
	    td = tr[i].getElementsByTagName("td")[2];
	    if (td) {
	      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
	        tr[i].style.display = "";
	      } else {
	        tr[i].style.display = "none";
	      }
	    }       
	  }
	}

function closeProject()
{
	if (confirm("Do you want to close this project")) {
	    return true;
	} else {
	    return false;
	}
}

function status()
{
	var select, radio, phase, table, tr, td, i;
	select = document.getElementsByName("status");
	for (i = 0; i < select.length; i++) {
	    if (select[i].checked) {
	        radio = select[i].value;
	    }
	}
	table = document.getElementById("myTable");
	tr = table.getElementsByTagName("tr");
	for (i = 0; i < tr.length; i++) {
		    td = tr[i].getElementsByTagName("td")[9];
		    if (td) {
		    	if (td.innerHTML.indexOf(radio) > -1) {
			        tr[i].style.display = "";
			      } else {
			        tr[i].style.display = "none";
			      }
		    	}
	}
}

function problem_flag()
{
	
}