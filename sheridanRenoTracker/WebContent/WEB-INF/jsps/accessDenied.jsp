<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html PUBLIC "-//W3C//Dth HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dth">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Sheridan Building Reno Tracker</title>
<link rel="stylesheet" href="<c:url value="/css/layout.css"/>"/>
<script src="<c:url value="/scripts/script.js"/>"></script>
</head>
<body>
<nav class="navbar navbar-custom">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"><span class="glyphicon glyphicon-th-list"></span></button>
      <c:url value="/secure" var="url"/><a href="${url}"><img src="<c:url value="/images/sheridanCollege.png"/>"  width="100" height="50" title="Logo" alt="Logo"/></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><c:url value="/logOff" var="url"/><a href="${url}"><span class="glyphicon glyphicon-log-out"></span>Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="container-fluid text-center">   
	<div class="row content">
	    <div class="col-sm-2 sidenav">
		    <c:if test= "${userRole == '[ROLE_ADMIN]'}">
		      <h2>Options</h2>
		      <p><c:url value="/createProject" var="url"/><a href="${url}">Create Project</a></p>
		      <p><c:url value="/adminAccess" var="url"/><a href="${url}">Admin Portal</a></p>
		      <p><c:url value="/updatePassword" var="url"/><a href="${url}">Update Password</a></p>
		      <p><c:url value="/secure" var="url"/><a href="${url}">Back</a></p>
		    </c:if>
		    <c:if test= "${userRole == '[ROLE_USER]'}">
		      <h2>Options</h2>
		      <p><a href="#">Search Project</a></p>
		      <p><a href="#">Update Password</a></p>
		      <p><a href="#">Back</a></p>
		    </c:if>
	    </div>
	    <div class="col-sm-8 text-center"> 
		   <h1 id="header">Warning</h1>
		<h2 id="innerContent">Sorry, you do not have permission to view this page.</h2>
		<h3><a href="secure">Back</a></h3>
		</div>
	</div>
</div>
<footer class="container-fluid text-center">
	     <ul class="nav navbar-nav navbar-right">
	        <li><c:url value="/contact" var="url"/><a href="${url}">Contact Admin</a></li>
	     </ul>		
	</footer>
</body>
</html>