<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html PUBLIC "-//W3C//Dth HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dth">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Sheridan Building Reno Tracker</title>
<link rel="stylesheet" href="<c:url value="/css/layout.css"/>"/>
<script src="<c:url value="/scripts/script.js"/>"></script>
</head>
<body>
<div>
<nav class="navbar navbar-custom">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"><span class="glyphicon glyphicon-th-list"></span></button>
      <c:url value="/secure" var="url"/><a href="${url}"><img src="<c:url value="/images/sheridanCollege.png"/>"  width="100" height="50" title="Logo" alt="Logo"/></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
     <ul class="nav navbar-nav">
	      <li class="nav-item dropdown">
	      	 <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	          Option
	       	 </a>
	       	 <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
		    	<li><c:url value="/searchProject" var="url"/><a class="dropdown-item" href="${url}">Search Project</a></li>
		      	<li><c:url value="/updatePassword" var="url"/><a class="dropdown-item" href="${url}">Update Password</a></li>
	       	 </ul>
	      </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><c:url value="/logOff" var="url"/><a href="${url}"><span class="glyphicon glyphicon-log-out"></span>Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="container-fluid text-center">   
	<div class="row content">
	    <div class="col-sm-2 sidenav">
	    </div> 
	    
	    <div class="col-sm-8 text-center"> 
	    	<h1 id="header">Contact Admin Page</h1>
	    	<c:if test="${addIssue != null}">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
    				<strong>Request Sent to Admin</strong>
  				</div>	
			</c:if>
			<div id="error"></div>
			<c:url value="/contactRequest" var="url"/>
			<form name="form" class="form-horizontal" method="post" action="${url}" onsubmit="return reviewVerify()">
				<div class="form-group">
				<label class="control-label col-sm-4" for="details">Details: </label>
					<div class="col-sm-5">
							<textarea class="form-control" rows="4" cols="50" name="description"/></textarea>
					</div>
				</div>
			<input type="submit" class="btn btn-default" value="Submit" onclick="verify()" /></form>
	    </div>
	  </div>
</div>
<footer class="container-fluid text-center">
	     <ul class="nav navbar-nav navbar-right">
	        <li>&copy; Copyright 2018</li>
	     </ul>		
</footer>
</body>
</html>