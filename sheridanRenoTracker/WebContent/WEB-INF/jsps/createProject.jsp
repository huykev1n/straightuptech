<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html PUBLIC "-//W3C//Dth HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dth">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Sheridan Building Reno Tracker</title>
<link rel="stylesheet" href="<c:url value="/css/layout.css"/>"/>
<script src="<c:url value="/scripts/script.js"/>"></script>
</head>
<body>
<nav class="navbar navbar-custom">
  <div class="container-fluid">
    <div class="navbar-header">
     <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"><span class="glyphicon glyphicon-th-list"></span></button>
      <c:url value="/secure" var="url"/><a href="${url}"><img src="<c:url value="/images/sheridanCollege.png"/>"  width="100" height="50" title="Logo" alt="Logo"/></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
 		<li class="nav-item dropdown">
	      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	         Option
	      </a>
	      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
      		<li><c:url value="/adminAccess" var="url"/><a href="${url}">Admin Portal</a></li>
      		<li><c:url value="/updatePassword" var="url"/><a href="${url}">Update Password</a></li>
	      </ul>
	     </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><c:url value="/logOff" var="url"/><a href="${url}"><span class="glyphicon glyphicon-log-out"></span>Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="container-fluid text-center">   
	<div class="row content">
	    <div class="col-sm-2 sidenav">
	     </div>
	      <div class="col-sm-8 text-center"> 
	      	<h1>Create Project</h1>
			<h2 id="innerContent" style="margin-right:0em;"></h2>
			<c:if test="${verify != null}">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
    				<strong>Project Created</strong>
  				</div>
			</c:if>
			<c:if test="${error == true}">
				<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
    				<strong>Project Number existed. Please use another one</strong>
  				</div>
			</c:if>
			<div id="error"></div>
			<c:url value="/addProject" var="url"/>
			<c:url value="/searchWing" var="url1"/>
			<c:url value="/searchRoom" var="url2"/>
			<form name="form" method="post" action="${url}" class="form-horizontal" onsubmit="return submitProject()">
				<div class="form-group">
					  <label class="control-label col-sm-2" for="Campus">Campus:</label>
					  <div class="col-sm-8"> 
						  <select class="form-control"  name="campus_list">
						  	<option value="All">All</option>
							<c:if test="${building_get != null}">
								<c:forEach var="selected_campus" items="${campuses_building}">
									<option value="${selected_campus}" selected>${selected_campus}</option>
								</c:forEach> 
							</c:if> 
							<c:forEach var="campus_list" items="${campus_building}">
								<option value="${campus_list}">
							<c:out value="${campus_list}"/>
							</option> 
							</c:forEach>
			  		  	  </select>
			  		  </div>
			  		  <div class="col-sm-2">
			  		  	<input class="btn btn-default" type="submit" formaction="${url1}" value="Search Wing"/>
			  		  </div>
				</div>
				<div class="form-group">
					  <label class="control-label col-sm-2" for="Wing">Wing:</label>
					  <div class="col-sm-8">
					  	<select class="form-control" name="wing_list">
								<option value="N/A">N/A</option>
								<option value="Site">Site</option>
								<c:if test="${wing_get != null}">
									<c:forEach var="selected_wing" items="${wing_buildings}">
										<option value="${selected_wing}" selected>${selected_wing}</option>
									</c:forEach>
								</c:if>  
									<c:forEach var="wing_list" items="${wing_building}">
										<option value="${wing_list}">
											<c:out value="${wing_list}"/>
										</option> 
									</c:forEach>
		  		  		</select>
					  </div>
					  <div class="col-sm-2">
					  	<input class="btn btn-default" type="submit" formaction="${url2}" value="Search Room">
					  </div>
				</div>
				<div class="form-group">
					  <label class="control-label col-sm-2" for="Room">Room:</label>
					  <div class="col-sm-8">
					  	<select class="form-control" name="room_list">
								<option value="N/A">N/A</option> 
									<c:forEach var="room_list" items="${room_building}">
										<option value="${room_list}">
											<c:out value="${room_list}"/>
										</option> 
									</c:forEach>
		  		  		</select>
					  </div>
				</div>
				<div class="form-group">
					  <label class="control-label col-sm-2" for="Project Manager">Project Manager:</label>
					  <div class="col-sm-8">
					  	<select class="form-control" name="project_manager">
								<option value="N/A">N/A</option> 
								<c:forEach var="project_managers" items="${project_managers}">
									<option value="${project_managers}">
										<c:out value="${project_managers}"/>
									</option> 
								</c:forEach>
		  		  		</select>
					  </div>
				</div>
				<div class="form-group">
					 <label class="control-label col-sm-2" for="project_number">Project Number:</label>
					 <div class="col-sm-2">
					 	<input class="form-control" type="text" name="project_num" maxlength="4"/>
					 </div>
					 <label class="control-label col-sm-2" for="project_name">Project Name:</label>
					 <div class="col-sm-4">
					 	<input class="form-control" name="project_name" type="text" />
					 </div>
				</div>
				<div class="form-group">
					 <label class="control-label col-sm-2" for="square_ft">Sqaure Ft:</label>
					 <div class="col-sm-2">
					 	<input class="form-control" name="square_ft" type="text" />
					 </div>
					 <label class="control-label col-sm-2" for="budget_cost">Budget Cost:</label>
					 <div class="col-sm-2">
					 	<input class="form-control" name="budget_cost" type="text" />
					 </div>
				</div>
				<div class="form-group">
					 <label class="control-label col-sm-2" for="department">Department</label>
					 <div class="col-sm-2">
					 	<input class="form-control" name="department" type="text" />
					 </div>
					 <label class="control-label col-sm-2" for="contact_name">Contact Name:</label>
					 <div class="col-sm-3">
					 	<input class="form-control" name="contact_name" type="text" />
					 </div>
				</div>	
					<input type="hidden" name="projected_cost" type="text" value="0" />				
					<input type="hidden" name="adjusted_cost" type="text" value="0" />
					<input type="hidden" name="project_phase" type="text" value="Initialized"/>
					<input type="hidden" name="vendor" type="text" value="N/A"/>
					<input type="hidden" name="percentage" type="text" value="0"/>
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /><br>
			<input type="submit" class="btn btn-default" value="Create New Project!" onclick="submitProject()"/></form><br>
			</div>
			<div class="col-sm-2 sidenav">
			
			</div>
	</div>
</div>
<footer class="container-fluid text-center">
	     <ul class="nav navbar-nav navbar-right">
	        <li>&copy; Copyright 2018</li>
	     </ul>		
</footer>
</body>
</html>