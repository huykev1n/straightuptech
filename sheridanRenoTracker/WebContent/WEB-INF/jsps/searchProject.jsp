<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html PUBLIC "-//W3C//Dth HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dth">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Sheridan Building Reno Tracker</title>
<link rel="stylesheet" href="<c:url value="/css/layout.css"/>"/>
<script src="<c:url value="/scripts/script.js"/>"></script>
</head>
<body>
<div>
<nav class="navbar navbar-custom">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"><span class="glyphicon glyphicon-th-list"></span></button>
      <c:url value="/secure" var="url"/><a href="${url}"><img src="<c:url value="/images/sheridanCollege.png"/>"  width="100" height="50" title="Logo" alt="Logo"/></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
	      <li class="nav-item dropdown">
	      	 <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	          Option
	       	 </a>
	       	 <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
	       	 <c:if test= "${userRole == '[ROLE_ADMIN]'}">
		      	<li><c:url value="/createProject" var="url"/><a class="dropdown-item" href="${url}">Create Project</a></li>
		      	<li><c:url value="/adminAccess" var="url"/><a class="dropdown-item" href="${url}">Admin Portal</a></li>
		      	<li><c:url value="/updatePassword" var="url"/><a class="dropdown-item" href="${url}">Update Password</a></li>
		     </c:if>
		     <c:if test= "${userRole == '[ROLE_USER]'}">
		      	<li><c:url value="/updatePassword" var="url"/><a class="dropdown-item" href="${url}">Update Password</a></li>
		     </c:if>
	       	 </ul>
	      </li>
      </ul>	
      <ul class="nav navbar-nav navbar-right">
        <li><c:url value="/logOff" var="url"/><a href="${url}"><span class="glyphicon glyphicon-log-out"></span>Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="container-fluid text-center">   
	<div class="row content">
	    <div class="col-sm-2 sidenav">
	    <c:if test= "${userRole == '[ROLE_ADMIN]'}">
	      <h3>Search Project Option</h3>
	      <c:if test="${toogle == 'True'}">
		  		<p><c:url value="/getAllProject" var="url"/><a href="${url}">Select All Project</a></p>
		  </c:if>
		  <c:if test="${toogle != 'True'}">
		  		<p><c:url value="/searchProject" var="url"/><a href="${url}">Select Project without Closed</a></p>
		  </c:if>
		  <h3>Project Status</h3>
		  <input type="radio" name="status" value="Initialized" onclick="return status()">Initialized<br>
		  <input type="radio" name="status" value="Design" onclick="return status()"> Design<br>
		  <input type="radio" name="status" value="Project Tendor" onclick="return status()"> Project Tendor<br>
		  <input type="radio" name="status" value="Execution" onclick="return status()"> Execution<br>
		  <input type="radio" name="status" value="Closed" onclick="return status()"> Closed<br>
		  <h3>Problem</h3>
		  <div class="btn-group btn-toggle float-right">
		  	<c:url value="/searchProjectProblem" var="url"/>
		  	<c:url value="/searchProjectProblems" var="url2"/>  	
		  	<c:choose>
    			<c:when test="${Problem == 'Yes'}">
    				<a href="${url}" class="btn btn-default">Yes</a>
			 		<a href="${url2}" class="btn btn-primary active">No</a>
    			</c:when>
    			<c:when test="${Problem == 'No'}">
    				<a href="${url}" class="btn btn-primary active">Yes</a>
			 	 	<a href="${url2}" class="btn btn-default">No</a>
    			</c:when>
    			<c:otherwise>
    				<a href="${url}" class="btn btn-default">Yes</a>
			 		<a href="${url2}" class="btn btn-primary active">No</a>
    			</c:otherwise>
			</c:choose>
		  </div>
		  
	    </c:if>
	    <c:if test= "${userRole == '[ROLE_USER]'}">
	     <h3>Project Status</h3>
	      <input type="radio" name="status" value="Initialized" onclick="return status()">Initialized<br>
		  <input type="radio" name="status" value="Design" onclick="return status()"> Design<br>
		  <input type="radio" name="status" value="Project Tendor" onclick="return status()"> Project Tendor<br>
		  <input type="radio" name="status" value="Execution" onclick="return status()"> Execution<br>
		  <input type="radio" name="status" value="Closed" onclick="return status()"> Closed<br>
		  <h2>Problem:</h2>
		  <div class="btn-group btn-toggle float-right">
		  	<c:url value="/searchProjectProblem" var="url"/>
		  	<c:url value="/searchProjectProblems" var="url2"/>
			  <a href="${url}" class="btn btn-default">Yes</a>
			  <a href="${url2}" class="btn btn-primary active">No</a>
		   </div>
	    </c:if>
	    </div> 
	    <div class="col-sm-10 text-center"> 
	      <h1>Search Project</h1>
	      <c:if test="${Submitted != null}">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
    				<strong>Project Updated</strong>
  				</div>
		  </c:if>
		  <c:if test="${close != null}">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
    				<strong>Project Closed</strong>
  				</div>
		  </c:if>
		  <c:if test="${details_added != null}">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
    				<strong>Project Details Added</strong>
  				</div>
		 </c:if>
		 <c:if test="${add_problem != null}">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
    				<strong>Problems Added</strong>
  				</div>
		 </c:if>  
	      <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for Project Manager" title="Type in a name">
	      <div class="table-responsive">
	      	<table id="myTable" style="margin:auto;">
				<tr>
					<th style="border:solid;text-align:center;"><h4>Project#</h4></th>
					<th style="border:solid;text-align:center;"><h4>Project Name</h4></th>
					<th style="border:solid;text-align:center;"><h4>Project Manager</h4></th>
					<th style="border:solid;text-align:center;width:8em;"><h4>Campus</h4></th>
					<th style="border:solid;text-align:center;width:8em;"><h4>Wing</h4></th>
					<th style="border:solid;text-align:center;width:8em;"><h4>Room</h4></th>
					<th style="border:solid;text-align:center;"><h4>Project Budget</h4></th>
					<th style="border:solid;text-align:center;"><h4>Estimate Cost</h4></th>
					<th style="border:solid;text-align:center;"><h4>Actual Cost</h4></th>
					<th style="border:solid;text-align:center;"><h4>Project Status</h4> </th>
					<th style="border:solid;text-align:center;"><h4>Percentage</h4></th>
					<c:choose>
						<c:when test= "${userRole == '[ROLE_ADMIN]'}">
							<th style="border:solid;text-align:center;"><h4>Update Project</h4></th>
						</c:when>
						<c:otherwise>
							<th style="border:solid;text-align:center;"><h4>Add Details</h4></th>
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test= "${userRole == '[ROLE_ADMIN]'}">
							<th style="border:solid;text-align:center;"><h4>Close</h4></th>
						</c:when>
						<c:otherwise>
							<th style="border:solid;text-align:center;"><h4>Vendor Details</h4></th>
						</c:otherwise>
					</c:choose>
				</tr>
				<c:forEach var="project" items="${Project}">
				<tr>
					<td style="border:solid;text-align:center;"><c:url value="/viewProjects/${project.project_num}" var="displayUrl"/><a href="${displayUrl}">${project.project_num}</a></td>
					<td style="border:solid;text-align:center;">${project.projectName}</td>
					<td style="border:solid;text-align:center;">${project.project_manager}</td>
					<td style="border:solid;text-align:center;">${project.building_name}</td>
					<td style="border:solid;text-align:center;">${project.wing_name}</td>
					<td style="border:solid;text-align:center;">${project.room}</td>
					<c:choose>
						<c:when test ="${project.actual_cost > project.budget || project.actual_cost > project.projected_cost}">
							<td style="border:solid;text-align:center;text-color:red;">$${project.budget}</td>
							<td style="border:solid;text-align:center;">$${project.actual_cost}</td>
							<td style="border:solid;text-align:center;">$${project.projected_cost}</td>
						</c:when>
						<c:otherwise>
							<td style="border:solid;text-align:center;">$${project.budget}</td>
							<td style="border:solid;text-align:center;">$${project.actual_cost}</td>
							<td style="border:solid;text-align:center;">$${project.projected_cost}</td>
						</c:otherwise>
					</c:choose>
					<td style="border:solid;text-align:center;">${project.project_phase}</td>
					<td style="border:solid;text-align:center;">${project.percentage}%</td>
					<c:choose>
						<c:when test= "${userRole == '[ROLE_ADMIN]'}">
							<td style="border:solid;text-align:center;"><c:url value="/updateProject/${project.id}" var="updateUrl"/><a href="${updateUrl}">Update</a></td>
						</c:when>
						<c:otherwise>
							<td style="border:solid;text-align:center;"><c:url value="/addContractorDetail/${project.id}" var="updateUrl"/><a href="${updateUrl}">Update</a></td>
						</c:otherwise>
					
					</c:choose>
					<c:choose>
						<c:when test= "${userRole == '[ROLE_ADMIN]'}">
							<td style="border:solid;text-align:center;"><c:url value="/closeProject/${project.id}" var="deleteUrl"/><a href="${deleteUrl}" onclick="return closeProject()">Close</a></td>
						</c:when>
						<c:otherwise>
							<td style="border:solid;text-align:center;"><c:url value="/viewContractorDetail/${project.id}" var="viewContractorDetail"/><a href="${viewContractorDetail}">Details</a></td>
						</c:otherwise>
					</c:choose>
					<c:if test= "${Problem == 'No'}">
					<td> <c:url value="/viewProblem/${project.projectName}" var="url"/><span class="glyphicon glyphicon-flag" style="margin-left:1em;" onclick="window.location.href='${url}'"></span></td>
					</c:if>
				</tr>
				</c:forEach>
			</table>
	      </div>
			<br>
				<c:url value="/secure" var="url"/><a href="${url}">Back</a><br><br>
	    </div>
	  </div>
</div>
</div>
	<footer class="container-fluid text-center">
	     <ul class="nav navbar-nav navbar-right">
	     	<li>&copy; Copyright 2018</li>
	     </ul>		
	</footer>
</body>
</html>