<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html PUBLIC "-//W3C//Dth HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dth">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Sheridan Building Reno Tracker</title>
<link rel="stylesheet" href="<c:url value="/css/layout.css"/>"/>
<script src="<c:url value="/scripts/script.js"/>"></script>
</head>
<body>
<div>
<nav class="navbar navbar-custom">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"><span class="glyphicon glyphicon-th-list"></span></button>
      <c:url value="/secure" var="url"/><a href="${url}"><img src="<c:url value="/images/sheridanCollege.png"/>"  width="100" height="50" title="Logo" alt="Logo"/></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
	      <li class="nav-item dropdown">
	      	 <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	          Option
	       	 </a>
	       	 <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
		    	<li><c:url value="/searchProject" var="url"/><a class="dropdown-item" href="${url}">Search Project</a></li>
		      	<li><c:url value="/updatePassword" var="url"/><a class="dropdown-item" href="${url}">Update Password</a></li>
	       	 </ul>
	      </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><c:url value="/logOff" var="url"/><a href="${url}"><span class="glyphicon glyphicon-log-out"></span>Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="container-fluid text-center">   
	<div class="row content">
	    <div class="col-sm-2 sidenav">
	    </div>
	    <div class="col-sm-8 text-center"> 
	    <h3>Add Contractor Detail</h3>
	    <c:url value="/addContractorDetail" var="url"/>
	    	<c:forEach var="projectDetails" items="${Project}">
	    		<form name="form" method="post" action="${url}" class="form-horizontal">
	    			 <input type="hidden" class="form-control" name="project_id" value="${projectDetails.id}">
	    			 <div class="form-group">
	    				  <label class="control-label col-sm-2" for="contractor_vendor">Contractor/Vendor:</label>
	    				  <div class="col-sm-10"> 
	    				  	<select class="form-control" name="contractor_vendor">
	    				  		<option value="N/A">N/A</option>
	    				  		<c:forEach var="contractor" items="${contractor}">
									<option value="${contractor}" selected>${contractor}</option>
								</c:forEach>
								<c:forEach var="vendor" items="${vendor}">
									<option value="${vendor}" selected>${vendor}</option>
								</c:forEach>
	    				  	</select>
	    				  </div>
	    			</div>
	    			<div class="form-group">
	    				  <label class="control-label col-sm-2" for="actual_cost">Actual Cost:</label>
	    				  <div class="col-sm-10">
	    				  	<input type="text" class="form-control" name="actual_cost" id="actual_cost" value="${projectDetails.actual_cost}" placeholder="Number Only">
	    				  </div>
	    			</div>
	    			<div class="form-group">
	    				  <label class="control-label col-sm-2" for="estimate_cost">Estimate Cost:</label>
	    				  <div class="col-sm-10">
	    				  	  <input type="text" class="form-control" name="estimate_cost" id="estimate_cost" value="${projectDetails.projected_cost}" placeholder="Number Only">	  
	    				  </div>
	    			</div>
	    			<div class="form-group">
    					<label class="control-label col-sm-2" for="work_description">Work Description:</label>
    					<div class="col-sm-10">
    						 <textarea class="form-control" id="work_description" name="work_description" rows="3"></textarea>
    					</div>
					</div>	
					<div class="form-group">
    					<label class="control-label col-sm-2"  for="work_note">Work Note:</label>
    					<div class="col-sm-10">
					    	<textarea class="form-control" id="work_note" name="work_note" rows="3"></textarea>
					    </div>
					</div>
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /><br>
					<input class="btn btn-default" type="submit" value="Add Project Detail!"/>	
	    		</form><br>
	    	</c:forEach>
	    </div>
	  </div>
</div>
</div>
	<footer class="container-fluid text-center">
	     <ul class="nav navbar-nav navbar-right">
	     	<li>&copy; Copyright 2018</li>
	     </ul>		
	</footer>
</body>
</html>