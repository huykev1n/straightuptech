<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html PUBLIC "-//W3C//Dth HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dth">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Sheridan Building Reno Tracker</title>
<link rel="stylesheet" href="<c:url value="/css/layout.css"/>"/>
<script src="<c:url value="/scripts/script.js"/>"></script>
</head>
<body>
<nav class="navbar navbar-custom">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"><span class="glyphicon glyphicon-th-list"></span></button>
      <c:url value="/secure" var="url"/><a href="${url}"><img src="<c:url value="/images/sheridanCollege.png"/>"  width="100" height="50" title="Logo" alt="Logo"/></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
         <li class="nav-item dropdown">
	      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	         Option
	      </a>
	      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
	      	<li><c:url value="/createProject" var="url"/><a href="${url}">Create Project</a></li>
      		<li><c:url value="/adminAccess" var="url"/><a href="${url}">Admin Portal</a></li>
      		<li><c:url value="/updatePassword" var="url"/><a href="${url}">Update Password</a></li>
	      </ul>
	     </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><c:url value="/logOff" var="url"/><a href="${url}"><span class="glyphicon glyphicon-log-out"></span>Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="container-fluid text-center">   
	<div class="row content">
	    <div class="col-sm-2 sidenav">
	    </div>
	    <div class="col-sm-8 text-center"> 
	     <h1 id="header">Update Project</h1>
		<h2 id="innerContent" style="margin-right:0em;"></h2>
			<c:if test="${verify != null}">
				<h3>Project Update</h3>
			</c:if>
			<div id="error" style="margin-left:0em;"></div>
			<c:forEach var="project" items="${Project}">
			<c:url value="/updateNewProject/${project.id}" var="url"/>
			<form name="form" class="form-horizontal" method="post" action="${url}" onsubmit="return submitProject()">
				<input type="hidden" name="campus_list" value="${project.building_name}"/>
				<input type="hidden" name="wing_list" value="${project.wing_name}"/>
				<input type="hidden" name="room_list" value="${project.room}"/>
				<div class="form-group">
					 <label class="control-label col-sm-5" for="project_num">Project Number:</label>
					 <div class="col-sm-2">
						<input class="form-control"  type="text" name="project_num" maxlength="4" value="${project.project_num}"/> 
					 </div>
				</div> 
				<div class="form-group">
					 <label class="control-label col-sm-5" for="project_name">Project Name:</label>
					 <div class="col-sm-3">
						<input class="form-control" type="text" name="project_name" type="hidden" value="${project.projectName}"/>
					 </div>
				</div> 
				<div class="form-group">
					 <label class="control-label col-sm-5" for="square_ft">Sqaure Ft:</label>
					 <div class="col-sm-2">
						<input class="form-control" type="text" name="square_ft" type="hidden" value="${project.square_ft}"/>
					 </div>
				</div> 
				<input name="department" type="hidden" value="${project.department}"/>
				<input name="contact_name" type="hidden" value="${project.contact_name}"/>
				<input name="budget_cost" pattern=".*\d" title="Alphanumeric letter not permitted" type="hidden" value="${project.budget}" />
				<input type="hidden" name="projected_cost" type="text" value="${project.projected_cost}"/>
				<input type="hidden" name="adjusted_cost" type="text" value="${project.actual_cost}"/>
				<div class="form-group">
					  <label class="control-label col-sm-5" for="Project_Manager">Project Manager:</label>
					  <div class="col-sm-2">
					  	<select class="form-control" name="project_manager">
							<option value="N/A">N/A</option>
								<c:forEach var="projectManager_list" items="${project_manager}">
									<option value="${projectManager_list}">
										<c:out value="${projectManager_list}"/>
									</option> 
								</c:forEach>
		  		  		</select>
					  </div>
				</div>
				<div class="form-group">
					  <label class="control-label col-sm-5" for="Vendor">Vendor:</label>
					  <div class="col-sm-2">
					  	<select class="form-control" name="vendor">
							<option value="N/A">N/A</option>
								<c:forEach var="vendor_list" items="${Vendor}">
									<option value="${vendor_list}">
										<c:out value="${vendor_list}"/>
									</option> 
								</c:forEach>
		  		  		</select>
					  </div>
				</div>
				<div class="form-group">
					 <label class="control-label col-sm-5" for="project_phase">Project Phase:</label>
					 <div class="col-sm-2">
					 	<select class="form-control" name="project_phase">
					 		<option value="Initialized">Initialized</option>
					 		<option value="Design">Design</option>
					 		<option value="Project Tendor">Project Tendor</option>
					 		<option value="Execution">Execution</option>
					 		<option value="Closed">Closed</option>
					 	</select>
					 </div>
				</div>
				<div class="form-group">
					 <label class="control-label col-sm-5" for="percentage">Percentage:</label>
					 <div class="col-sm-2">
					 		<select class="form-control" name="percentage">
					 			<option value="0">0</option>
					 			<option value="20">20</option>
					 			<option value="40">40</option>
					 			<option value="60">60</option>
					 			<option value="80">80</option>
					 			<option value="100">100</option>
					 		</select>
					 </div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-5" for="problem">Problem:</label>
					<div class="col-sm-2">
						<label class="radio-inline"><input type="radio" name="problem" value="enabled">Yes</label>
						<label class="radio-inline"><input type="radio" name="problem" value="no">No</label>
					</div>
				</div>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /><br>
		<input type="submit" class="btn btn-default" value="Update Project!" onclick="submitProject()"/>
		</form><br>
		</c:forEach>
		</div>
		<div class="col-sm-2 sidenav"></div>
	</div>
</div>
<footer class="container-fluid text-center">
	     <ul class="nav navbar-nav navbar-right">
	    	<li>&copy; Copyright 2018</li>
	     </ul>		
</footer>
</body>
</html>