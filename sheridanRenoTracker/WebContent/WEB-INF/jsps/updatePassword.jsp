<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html PUBLIC "-//W3C//Dth HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dth">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Sheridan Building Reno Tracker</title>
<link rel="stylesheet" href="<c:url value="/css/layout.css"/>"/>
<script src="<c:url value="/scripts/script.js"/>"></script>
</head>
<body>
<nav class="navbar navbar-custom">
  <div class="container-fluid">
    <div class="navbar-header">
     <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"><span class="glyphicon glyphicon-th-list"></span></button>
      <c:url value="/secure" var="url"/><a href="${url}"><img src="<c:url value="/images/sheridanCollege.png"/>"  width="100" height="50" title="Logo" alt="Logo"/></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
      	<li class="nav-item dropdown">
	      	 <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	          Option
	       	 </a>
	       	 <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
	       	 <c:if test= "${userRole == '[ROLE_ADMIN]'}">
		      	<li><c:url value="/createProject" var="url"/><a class="dropdown-item" href="${url}">Create Project</a></li>
		      	<li><c:url value="/adminAccess" var="url"/><a class="dropdown-item" href="${url}">Admin Portal</a></li>
		     </c:if>
		     <c:if test= "${userRole == '[ROLE_USER]'}">
		    	<li><c:url value="/searchProject" var="url"/><a class="dropdown-item" href="${url}">Search Project</a></li>
		     </c:if>
	       	 </ul>
	      </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><c:url value="/logOff" var="url"/><a href="${url}"><span class="glyphicon glyphicon-log-out"></span>Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="container-fluid text-center">   
	<div class="row content">
	    <div class="col-sm-2 sidenav">
	    </div>
	    <div class="col-sm-8 text-center"> 
	     <h1 id="header">Update Password</h1>
		<h2 id="innerContent" style="margin-right:0em;"></h2>
			<c:if test="${verify != null}">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
    				<strong>Password Update</strong>
  				</div>
			</c:if>
			<div>
				<strong id="error"></strong>
			</div>
			<br>
			<c:url value="/updateNewPassword" var="url"/>
			<form class="form-signin" name="form" method="post" action="${url}" onsubmit="return verify()">
			<div align="center">
					<input style="width:20em;" type="password" class="form-control" type="password" placeholder="New Password" name="password"/><br>
					<input style="width:20em;" type="password" class="form-control" type="password" placeholder="Confirm Password" name="verifyPassword"/>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /><br>
		<input type="submit" class="btn btn-default" value="Update Password" onclick="verify()"/>
		</div>
		</form><br>
		</div>
		 <div class="col-sm-2 sidenav"></div>
	</div>
</div>
<footer class="container-fluid text-center">
	     <ul class="nav navbar-nav navbar-right">
	        <li>&copy; Copyright 2018</li>
	     </ul>		
</footer>
</body>
</html>