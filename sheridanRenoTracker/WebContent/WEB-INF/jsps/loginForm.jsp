<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri ="http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sheridan Building Reno Tracker</title>
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="<c:url value="/css/style.css"/>">
<script src="<c:url value="/scripts/script.js" />"></script>
<link rel="stylesheet" href="<c:url value="/css/layout.css"/>"/>
</head>
<body>
<nav class="navbar navbar-custom">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"><span class="glyphicon glyphicon-th-list"></span></button>
      <c:url value="/" var="url"/><a href="${url}"><img src="<c:url value="/images/sheridanCollege.png"/>"  width="100" height="50" title="Logo" alt="Logo"/></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><c:url value="/secure" var="url"/><a href="${url}"><span class="glyphicon glyphicon-log-out"></span>Login</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="container-fluid text-center">   
	<div class="row content">
	    <div class="col-sm-2 sidenav">
		   
	    </div>
	    <div class="col-sm-8 text-center"> 
		 	<h3 style="text-align:center;">Please login</h3>
			<c:if test="${param.error != null}">
				<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>No such account exists.  Please check your username and password!</strong>
				</div>
			</c:if>
			<c:url value="/login" var="loginUrl"/>
			<form class="form-signin" method="post" action="${loginUrl}">
			<div align="center">
					<input style="width:20em;" type="text" class="form-control" name="username" placeholder="Username"/>
					<br>
					<input style="width:20em;" type="password" class="form-control" name="password" placeholder="Password">
			
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/><br>
				<input type="submit" class="btn btn-primary btn-lg" value="Login!"/>
			</div>
			</form>
			<h3><c:url value="/" var="homeUrl"/><a href="${homeUrl}">Back Home Page</a></h3>
		</div>
		<div class="col-sm-2 sidenav">
		
		</div>
	</div>
</div>
<footer class="container-fluid text-center">
	     <ul class="nav navbar-nav navbar-right">
	        <li>&copy; Copyright 2018</li>
	     </ul>		
</footer>
</body>
</html>