<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html PUBLIC "-//W3C//Dth HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dth">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Sheridan Building Reno Tracker</title>
<link rel="stylesheet" href="<c:url value="/css/layout.css"/>"/>
<script src="<c:url value="/scripts/script.js"/>"></script>
</head>
<body>
<div>
<nav class="navbar navbar-custom">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"><span class="glyphicon glyphicon-th-list"></span></button>
      <c:url value="/secure" var="url"/><a href="${url}"><img src="<c:url value="/images/sheridanCollege.png"/>"  width="100" height="50" title="Logo" alt="Logo"/></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
     <ul class="nav navbar-nav">
         <li class="nav-item dropdown">
	      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	         Option
	      </a>
	      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
	      	<li><c:url value="/createProject" var="url"/><a href="${url}">Create Project</a></li>
	      	<li><c:url value="/searchProject" var="url"/><a href="${url}">Search Project</a></li>
      		<li><c:url value="/adminAccess" var="url"/><a href="${url}">Admin Portal</a></li>
      		<li><c:url value="/updatePassword" var="url"/><a href="${url}">Update Password</a></li>
	      </ul>
	     </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><c:url value="/logOff" var="url"/><a href="${url}"><span class="glyphicon glyphicon-log-out"></span>Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="container-fluid text-center">   
	<div class="row content">
	    <div class="col-sm-2 sidenav"></div> 
	    <div class="col-sm-8"> 
	    <h1 id="header">View Project Detail</h1>
	    <c:forEach var="project" items="${Project}">
	     <div class="table-responsive">
	      	<table id="myTable" style="margin:auto;">
				<tr>
					<th style="border:solid;text-align:center;"><h4>Project#</h4></th>
					<th style="border:solid;text-align:center;"><h4>Project Name</h4></th>
					<th style="border:solid;text-align:center;"><h4>Project Manager</h4></th>
					<th style="border:solid;text-align:center;width:8em;"><h4>Campus</h4></th>
					<th style="border:solid;text-align:center;width:8em;"><h4>Wing</h4></th>
					<th style="border:solid;text-align:center;width:8em;"><h4>Room</h4></th>
					<th style="border:solid;text-align:center;"><h4>Project Budget</h4></th>
					<th style="border:solid;text-align:center;"><h4>Projected Cost</h4></th>
					<th style="border:solid;text-align:center;"><h4>Actual Cost</h4></th>
					<th style="border:solid;text-align:center;"><h4>Project Status</h4> </th>
					<th style="border:solid;text-align:center;"><h4>Percentage</h4></th>
				</tr>
				<tr>
					<td style="border:solid;text-align:center;">${project.project_num}</a></td>
					<td style="border:solid;text-align:center;">${project.projectName}</td>
					<td style="border:solid;text-align:center;">${project.project_manager}</td>
					<td style="border:solid;text-align:center;">${project.building_name}</td>
					<td style="border:solid;text-align:center;">${project.wing_name}</td>
					<td style="border:solid;text-align:center;">${project.room}</td>
					<td style="border:solid;text-align:center;">$${project.budget}</td>
					<td style="border:solid;text-align:center;">$${project.actual_cost}</td>	
					<td style="border:solid;text-align:center;">$${project.projected_cost}</td>
					<td style="border:solid;text-align:center;">${project.project_phase}</td>
					<td style="border:solid;text-align:center;">${project.percentage}%</td>
				</tr>
			</table>
	      </div>
	      	</c:forEach>
	      </div>
	    <div class="col-sm-2 sidenav"></div>
	</div>
</div>
<footer class="container-fluid text-center">
	     <ul class="nav navbar-nav navbar-right">
	        <li><c:url value="/contact" var="url"/><a href="${url}">Contact Admin</a></li>
	     </ul>		
</footer>
</body>
</html>