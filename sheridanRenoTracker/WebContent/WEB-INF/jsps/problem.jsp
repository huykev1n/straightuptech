<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Sheridan Building Reno Tracker</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <title>Sheridan Building Reno Tracker</title>
  <link rel="stylesheet" href="<c:url value="/css/layout.css"/>"/>
  <script src="<c:url value="/scripts/script.js"/>"></script>
</head>
<body>

<nav class="navbar navbar-custom">
  <div class="container-fluid">
    <div class="navbar-header">
     <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"><span class="glyphicon glyphicon-th-list"></span></button>
      <c:url value="/secure" var="url"/><a href="${url}"><img src="<c:url value="/images/sheridanCollege.png"/>"  width="100" height="50" title="Logo" alt="Logo"/></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
     <ul class="nav navbar-nav">
         <li class="nav-item dropdown">
	      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	         Option
	      </a>
	      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
	      	<li><c:url value="/createProject" var="url"/><a href="${url}">Create Project</a></li>
      		<li><c:url value="/adminAccess" var="url"/><a href="${url}">Admin Portal</a></li>
      		<li><c:url value="/updatePassword" var="url"/><a href="${url}">Update Password</a></li>
	      </ul>
	     </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-log-out"></span>Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
    </div>
    <div class="col-sm-8 text-center"> 
      <h1 style="text-align:center;text-decoration:underline;">Problem Page</h1>
      <c:url value="/addProblem" var="url"/>
		<form name="form" method="post" action="${url}" onsubmit="return submitProject()">
			<h3 style="text-align:center;">Enter your issue here:</h3>
			<table align="center">
			<c:forEach var="project" items="${project_name}">
						<input type="hidden" name="project_name" name="" value="${project}">
					</c:forEach>
					<c:forEach var="username" items="${username}">
						<input type="hidden" name="username" value="${username}">
			</c:forEach>
				<tr>
					<td>Details: </td>
					<td><textarea rows="4" cols="50" name="description"/></textarea></td>
				</tr>
			</table>
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /><br>
			<input type="submit" value="Submit Entry!" onclick="submitProject()"/>   	
		</form>
    </div>
  </div>
</div>
<footer class="container-fluid text-center">
	     <ul class="nav navbar-nav navbar-right">
	       	<li>&copy; Copyright 2018</li>
	     </ul>		
</footer>

</body>
</html>
