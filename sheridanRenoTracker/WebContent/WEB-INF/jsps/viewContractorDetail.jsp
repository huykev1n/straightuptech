<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html PUBLIC "-//W3C//Dth HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dth">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Sheridan Building Reno Tracker</title>
<link rel="stylesheet" href="<c:url value="/css/layout.css"/>"/>
<script src="<c:url value="/scripts/script.js"/>"></script>
</head>
<body>
<div>
<nav class="navbar navbar-custom">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"><span class="glyphicon glyphicon-th-list"></span></button>
      <c:url value="/secure" var="url"/><a href="${url}"><img src="<c:url value="/images/sheridanCollege.png"/>"  width="100" height="50" title="Logo" alt="Logo"/></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
     <ul class="nav navbar-nav">
	      <li class="nav-item dropdown">
	      	 <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	          Option
	       	 </a>
	       	 <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
		    	<li><c:url value="/searchProject" var="url"/><a class="dropdown-item" href="${url}">Search Project</a></li>
		      	<li><c:url value="/updatePassword" var="url"/><a class="dropdown-item" href="${url}">Update Password</a></li>
	       	 </ul>
	      </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><c:url value="/logOff" var="url"/><a href="${url}"><span class="glyphicon glyphicon-log-out"></span>Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="container-fluid text-center">   
	<div class="row content">
	    <div class="col-sm-2 sidenav">
	    </div> 
	    
	    <div class="col-sm-10 text-center"> 
	    	<h1>Contractors Details</h1>
	    	 <table id="myTable" style="margin:auto;">
				<tr>
					<th style="border:solid;text-align:center;width:10em;"><h4>Vendor/Contractor</h4></th>
					<th style="border:solid;text-align:center;width:20em;"><h4>Work Description</h4></th>
					<th style="border:solid;text-align:center;width:10em;"><h4>Estimate Cost</h4></th>
					<th style="border:solid;text-align:center;width:10em;"><h4>Actual Cost</h4></th>
					<th style="border:solid;text-align:center;width:20em;"><h4>Work Notes</h4></th>
				</tr>
				<c:forEach var="details" items="${detailList}">
				<tr>
					<td style="border:solid;text-align:center;">${details.vendor_contractor}</td>
					<td style="border:solid;text-align:center;">${details.work_details}</td>
					<td style="border:solid;text-align:center;">${details.estimate_cost}</td>
					<td style="border:solid;text-align:center;">${details.actual_cost}</td>
					<td style="border:solid;text-align:center;">${details.work_note}</td>
				</c:forEach>
			</table>
	    </div>
	  </div>
</div>
</div>
	<footer class="container-fluid text-center">
	     <ul class="nav navbar-nav navbar-right">
	       <li>&copy; Copyright 2018</li>
	     </ul>		
	</footer>
</body>
</html>