<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html PUBLIC "-//W3C//Dth HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dth">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Sheridan Building Reno Tracker</title>
<link rel="stylesheet" href="<c:url value="/css/layout.css"/>"/>
<script src="<c:url value="/scripts/script.js"/>"></script>
</head>
<body>
<div>
<nav class="navbar navbar-custom">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"><span class="glyphicon glyphicon-th-list"></span></button>
      <c:url value="/secure" var="url"/><a href="${url}"><img src="<c:url value="/images/sheridanCollege.png"/>"  width="100" height="50" title="Logo" alt="Logo"/></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
         <li class="nav-item dropdown">
	      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	         Option
	      </a>
	      <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
	      	<li><c:url value="/createProject" var="url"/><a href="${url}">Create Project</a></li>
      		<li><c:url value="/adminAccess" var="url"/><a href="${url}">Admin Portal</a></li>
      		<li><c:url value="/updatePassword" var="url"/><a href="${url}">Update Password</a></li>
	      </ul>
	     </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><c:url value="/logOff" var="url"/><a href="${url}"><span class="glyphicon glyphicon-log-out"></span>Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="container-fluid text-center">   
	<div class="row content">
	    <div class="col-sm-2 sidenav">
	    </div>  
	    <div class="col-sm-8 text-center"> 
	<h1 id="header">Create New Room</h1>
	<h2 id="innerContent" style="margin-right:0em;"></h2>
	<c:if test="${verify != null}">
		<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
    			<strong>Room Existed. Please try again</strong>
  		</div>	
	</c:if>
	<c:if test="${addRoom != null}">
		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
    		<strong>Room Created</strong>
  		</div>	
	</c:if>
	<c:if test="${building != null}">
		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
    		<strong>Room Error. Please try again</strong>
  		</div>	
	</c:if>
	<div id="error"></div>
	<c:url value="/addRoom" var="url"/>
	<c:url value="/searchWingBasedOnCampus" var="url1"/>
	<form name="form" class="form-horizontal" method="post" action="${url}" onsubmit="return verify()">
		<div class="form-group">
			<label class="control-label col-sm-5" for="campus">Campus:</label>
			<div class="col-sm-3">
				<select class="form-control"  name="campus_list">
					<option value="N/A">N/A</option> 
						<c:forEach var="campuses" items="${campuses_building}">
							<option value="${campuses}" selected>
								<c:out value="${campuses}"/>
							</option> 
						</c:forEach>
						<c:forEach var="campus_list" items="${campus_building}">
							<option value="${campus_list}">
								<c:out value="${campus_list}"/>
							</option> 
						</c:forEach>
  		  		</select>
			</div>
			 <div class="col-sm-2">
			 	<input class="btn btn-default" type="submit" formaction="${url1}" value="Search Wing"/>
			 </div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-5" for="wing">Campus Wing:</label>
			<div class="col-sm-3">
				<select class="form-control" name="wing_name">
					<option value="N/A">N/A</option> 
						<c:forEach var="wing_list" items="${wing_list}">
							<option value="${wing_list}">
								<c:out value="${wing_list}"/>
							</option> 
						</c:forEach>
  		  		</select>
			</div>
		</div> 
		<div class="form-group">
			<label class="control-label col-sm-5" for="room">Room:</label>
			<div class="col-sm-3">
				<input class="form-control" name="room_num" type="text" />
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-5" for="addtional_notes">Addtional Notes:</label>
			<div class="col-sm-3">
				<textarea class="form-control" name="notes" type="text" rows="3" cols="50" /></textarea>
			</div>
		</div> 
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /><br>
	<input class="btn btn-default" type="submit" value="Create New Room!" onclick="verify()" /></form><br>
	   </div>
	   <div class="col-sm-2 sidenav"></div>
	  </div>
</div>
</div>
	<footer class="container-fluid text-center">
	     <ul class="nav navbar-nav navbar-right">
	    	<li>&copy; Copyright 2018</li>
	     </ul>		
	</footer>
</body>
</html>