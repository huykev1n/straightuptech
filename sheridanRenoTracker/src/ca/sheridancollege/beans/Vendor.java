package ca.sheridancollege.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "vendor")
public class Vendor {
	@Id
	@Column(name = "name", unique = true, nullable = false, length = 45)
	private String name;
	@Column(name = "telphone", nullable = false, length = 60)
	private String telephone;
	@Column(name = "notes", length = 199)
	private String notes;
	
	public Vendor(String name, String telephone, String notes) {
		super();
		this.name = name;
		this.telephone = telephone;
		this.notes = notes;
	}

}

