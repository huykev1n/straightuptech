package ca.sheridancollege.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
//@AllArgsConstructor
@Entity
@Table(name = "VendorReview")
@NamedQuery(name="GetVendorsReview", query="from VendorReview where vendor =:vendor")
public class VendorReview {
	@Id
	@GeneratedValue
	private int id;
	@Column(name = "vendor", nullable = false, length = 45)
	private String vendor;
	@Column(name = "description", nullable = false, length = 255)
	private String description; 
	
	public VendorReview(String vendor, String description) {
		super();
		this.vendor = vendor;
		this.description = description;
	}

}
