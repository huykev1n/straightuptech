package ca.sheridancollege.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
//@AllArgsConstructor
@Entity
@Table(name = "ContractorReview")
@NamedQuery(name="GetContractor", query="from ContractorReview where contractor =:contractor")
public class ContractorReview {
	@Id
	@GeneratedValue
	private int id;
	@Column(name = "contractor", nullable = false, length = 45)
	private String contractor;
	@Column(name = "description", nullable = false, length = 255)
	private String description; 
	
	public ContractorReview(String contractor, String description) {
		super();
		this.contractor = contractor;
		this.description = description;
	}
}
