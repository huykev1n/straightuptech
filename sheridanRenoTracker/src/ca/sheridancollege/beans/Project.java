package ca.sheridancollege.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "project")
@NamedQueries({
	@NamedQuery(name="Project.getAllProject", query="from Project"),
	@NamedQuery(name="verifyProject", query="from Project where project_num =:project_num"),
	@NamedQuery(name="Project.getProjects", query="from Project where project_phase !='Closed' AND problem ='No'"),
	@NamedQuery(name="Project.getAllProjectProblem", query="from Project where problem ='Yes'"),
	@NamedQuery(name="Project.getAllProjectProblems", query="from Project where problem ='No'"),
	@NamedQuery(name="Project.getProjectByProjectNum", query="from Project where project_num =:project_num"),
	@NamedQuery(name="Project.getProjectById", query="from Project where id =:id"),
	})
public class Project {
	@Id
	@GeneratedValue
	private int id;
	@Column(name = "campus", nullable = false, length = 45)
	private String building_name;
	@Column(name = "wing", nullable = false, length = 45)
	private String wing_name;
	@Column(name = "room", nullable = false, length = 45)
	private String room;
	@Column(name = "project_num", nullable = false, length = 45)
	private int project_num;
	@Column(name = "projectName", nullable = false, length = 45)
	private String projectName;
	@Column(name = "square_ft", nullable = false, length = 45)
	private String square_ft;
	@Column(name = "department", nullable = false, length = 45)
	private String department;
	@Column(name = "contact_name", nullable = false, length = 45)
	private String contact_name;
	@Column(name = "budget", nullable = false, length = 45)
	private String budget;
	@Column(name = "projected_cost", nullable = false, length = 45)
	private String projected_cost;
	@Column(name = "actual_cost", nullable = false, length = 45)
	private String actual_cost;
	@Column(name = "project_phase", nullable = false, length = 45)
	private String project_phase;
	@Column(name = "percentage", nullable = false, length = 45)
	private String percentage;
	@Column(name = "project_manager", nullable = false, length = 45)
	private String project_manager;
	@Column(name = "vendor", nullable = false, length = 45)
	private String vendor;
	@Column(name = "problem", nullable = false, length = 45)
	private String problem;
	public Project(String building, String wing_name , String room, int project_num,
			String projectName, String square_ft, String department, String contact_name,
			String budget, String projected_cost, String actual_cost, String percentage,
			String project_phase, String project_manager, String problem, String vendor) {
		super();
		this.building_name = building;
		this.wing_name = wing_name;
		this.room = room;
		this.project_num = project_num;
		this.projectName = projectName;
		this.square_ft = square_ft;
		this.department = department; 
		this.contact_name = contact_name; 
		this.budget = budget;
		this.projected_cost = projected_cost;
		this.actual_cost = actual_cost;
		this.project_phase = project_phase;
		this.percentage = percentage;
		this.project_manager = project_manager;
		this.problem = problem;
		this.vendor = vendor;
	}

}
