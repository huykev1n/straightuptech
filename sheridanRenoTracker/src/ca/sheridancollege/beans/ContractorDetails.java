package ca.sheridancollege.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
//@AllArgsConstructor
@Entity
@Table(name = "ContractorDetails")
@NamedQuery(name="Contractor.getDetailsProjectName", query="from ContractorDetails WHERE project_id =:project_id")
public class ContractorDetails {
	@Id
	@GeneratedValue
	private int id;
	@Column(name = "project_id", nullable = false, length = 45)
	private String project_id;
	@Column(name = "vendor_contractor", nullable = false, length = 45)
	private String vendor_contractor;
	@Column(name = "work_details", nullable = false, length = 125)
	private String work_details;
	@Column(name = "estimate_cost", nullable = false, length = 45)
	private String estimate_cost;
	@Column(name = "actual_cost", nullable = false, length = 45)
	private String actual_cost;
	@Column(name = "work_note", nullable = false, length = 125)
	private String work_note;
	
	public ContractorDetails(String project_id, String contractor, String work_details,
			String estimate_cost, String actual_cost, String work_note)
	{
		super();
		this.project_id = project_id;
		this.vendor_contractor = contractor;
		this.work_details = work_details;
		this.estimate_cost = estimate_cost;
		this.actual_cost = actual_cost;
		this.work_note = work_note;
	}

}
