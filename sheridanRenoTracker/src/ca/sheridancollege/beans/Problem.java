package ca.sheridancollege.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
//@AllArgsConstructor
@Entity
@Table(name = "Problem")
@NamedQuery(name="Problem.getProjectByProjectName", query="from Problem WHERE projectName =:projectName")
public class Problem {
	@Id
	@GeneratedValue
	private int id;
	@Column(name = "projectName", nullable = false, length = 45)
	private String projectName;
	@Column(name = "username", nullable = false, length = 45)
	private String username; 
	@Column(name = "problem_description", nullable = false, length = 999)
	private String problem_description;
	
	public Problem(String projectName, String username, String problem_description)
	{
		super();
		this.projectName = projectName;
		this.username = username;
		this.problem_description = problem_description;
	}
}
