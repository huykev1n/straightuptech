package ca.sheridancollege.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
//@AllArgsConstructor
@Entity
@Table(name = "Contractor")
public class Contractor {
	@Id
	@GeneratedValue
	private int id;
	@Column(name = "first_name", nullable = false, length = 45)
	private String first_name;
	@Column(name = "last_name", nullable = false, length = 45)
	private String last_name;
	@Column(name = "email", nullable = false, length = 45)
	private String email;
	@Column(name = "telphone", nullable = false, length = 60)
	private String telephone;
	@Column(name = "notes", length = 199)
	private String notes;
	
	public Contractor(String first_name, String last_name, String email, String telephone, String notes) {
		super();
		this.first_name = first_name;
		this.last_name = last_name;
		this.email = email;
		this.telephone = telephone;
		this.notes = notes;
	}
	
}


