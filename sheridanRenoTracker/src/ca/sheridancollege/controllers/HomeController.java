package ca.sheridancollege.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ca.sheridancollege.beans.Building;
import ca.sheridancollege.beans.Contractor;
import ca.sheridancollege.beans.ContractorDetails;
import ca.sheridancollege.beans.ContractorReview;
import ca.sheridancollege.beans.Problem;
import ca.sheridancollege.beans.Project;
import ca.sheridancollege.beans.Room;
import ca.sheridancollege.beans.User;
import ca.sheridancollege.beans.UserRole;
import ca.sheridancollege.beans.Vendor;
import ca.sheridancollege.beans.VendorReview;
import ca.sheridancollege.beans.Wing;
import ca.sheridancollege.dao.DAO;

@Controller
public class HomeController {
	DAO dao = new DAO();
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String home(Model model)
	{
		SecurityContextHolder.getContext().setAuthentication(null);
		return "home";
	}
	
	@RequestMapping(value="/home", method=RequestMethod.GET)
	public String home_page(Model model)
	{
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		model.addAttribute("userRole", role);
		return "secure";
	}
	
	@RequestMapping(value="/homePage", method=RequestMethod.GET)
	public String backToHomePage(Model model)
	{
		return "home";
	}
	
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String logIn(Model model)
	{
		return "loginForm";
	}
	
	@RequestMapping(value="/logOff", method=RequestMethod.GET)
	public String logOff(Model model)
	{
		SecurityContextHolder.getContext().setAuthentication(null);
		model.addAttribute("logOff",true);
		return "home";
	}
	
	@RequestMapping(value="/accessDenied", method=RequestMethod.GET)
	public String accessDenied(Model model)
	{
		return "accessDenied";
	}


	@RequestMapping(value="/errorPage", method=RequestMethod.GET)
	public String errorPage(Model model)
	{
		return "errorPage";
	}
	
	@RequestMapping(value="/updateNewPassword", method=RequestMethod.POST)
	public String updateNewPassword(Model model, @RequestParam String password)
	{
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		model.addAttribute("userName", userName);
		
		String encryptedPassword = new BCryptPasswordEncoder().encode(password);
		
		dao.updatePassword(userName,encryptedPassword);
		
		model.addAttribute("verify", "True");
		model.addAttribute("userRole", role);
		return "updatePassword";
	}
	
	@RequestMapping(value="/secure", method=RequestMethod.GET)
	public String secure(Model model)
	{
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		model.addAttribute("userName", userName);
		model.addAttribute("userRole", role);
		return "secure";
	}
	
	@RequestMapping(value="/createAccount", method=RequestMethod.GET)
	public String createAccount(Model  model) 
	{
		return "createAccount";
	}
	
	@RequestMapping(value="/register",  method=RequestMethod.POST)
	public String register(Model model, @RequestParam String username, @RequestParam String password, @RequestParam String role)
	{
		System.out.println(role);
		String encryptedPassword = new BCryptPasswordEncoder().encode(password);
		User user = new User(username, encryptedPassword, true);
		
		UserRole userRole = new UserRole(user,role);
		user.getUserRole().add(userRole);
		
		dao.createUser(user);
		
		model.addAttribute("users_login", user);
		return "createAccount";
	}
	
	@RequestMapping(value="/addBuilding",  method=RequestMethod.POST)
	public String building(Model model, @RequestParam String building, @RequestParam String location, @RequestParam String notes)
	{
		List<Building> user_entries = dao.findBuilding(building);
		
		if(!user_entries.isEmpty()) {
			model.addAttribute("verify", user_entries);
			return "createBuilding";
		}
		else {
			Building campus = new Building(building, location, notes);
			dao.addBuilding(campus);
			
		    model.addAttribute("addCampus", campus);
			return "createBuilding";
		}
	}
	
	@RequestMapping(value="/addRoom",  method=RequestMethod.POST)
	public String room(Model model, @RequestParam String campus_list, @RequestParam String wing_name, @RequestParam String room_num , @RequestParam String notes)
	{
		List<Wing> findBuilding = dao.findInfo(campus_list,wing_name);		
		List<Room> user_entries = dao.findRoom(campus_list,wing_name, room_num);
		
		if(!findBuilding.isEmpty()) {
			if(!user_entries.isEmpty()) {
				
				List<Building> campuses = dao.findAllBuilding();
				List<Wing> wing = dao.findAllWing();
				
				model.addAttribute("campus_building", campuses);
				model.addAttribute("wing_list", wing);
				model.addAttribute("verify", user_entries);
				return "createRoom";
			}
			else {
				Room rooms = new Room(campus_list,wing_name, room_num, notes);	
				dao.addRooms(rooms);
				
				List<Building> campuses = dao.findAllBuilding();
				List<Wing> wing = dao.findAllWing();
				
				model.addAttribute("campus_building", campuses);
				model.addAttribute("wing_list", wing);
				model.addAttribute("addRoom", rooms);
				return "createRoom";
			}
		}
		else
		{
			List<Building> campuses = dao.findAllBuilding();
			List<Wing> wing = dao.findAllWing();
			
			model.addAttribute("campus_building", campuses);
			model.addAttribute("wing_list", wing);
			model.addAttribute("building", findBuilding);
			return "createRoom";
		}
	}
	
	@RequestMapping(value="/addWing",  method=RequestMethod.POST)
	public String wing(Model model, @RequestParam String campus_list, @RequestParam String wing_name, @RequestParam String notes)
	{
		List<Wing> user_entries = dao.findInfo(campus_list,wing_name);
		
		if(!user_entries.isEmpty()) {
			List<Building> campuses = dao.findAllBuilding();

			model.addAttribute("campus_building", campuses);
			model.addAttribute("verify", user_entries);
			return "createWing";
		}
		else {
			Wing wing = new Wing(wing_name,campus_list,notes);
			dao.addWing(wing);
			
			List<Building> campuses = dao.findAllBuilding();
		
			model.addAttribute("campus_building", campuses);
			model.addAttribute("addWing", wing);
			return "createWing";
		}
		
	}
	

	@RequestMapping(value="/addProject", method=RequestMethod.GET)
	public String addProject(Model model)
	{
		return "addProject";
	}
	
	@RequestMapping(value="/manual1", method=RequestMethod.GET)
	public String manual1(Model model)
	{
		return "manualAddProject";
	}
	
	@RequestMapping(value="/manual3", method=RequestMethod.GET)
	public String manual3(Model model)
	{
		return "manualCloseProject";
	}
	
	@RequestMapping(value="/manual4", method=RequestMethod.GET)
	public String manual4(Model model)
	{
		return "manualContactAdmin";
	}
	
	@RequestMapping(value="/addAdditionalProblem", method=RequestMethod.GET)
	public String addProblems(Model model)
	{
		return "problem";
	}
	
	@RequestMapping(value="/viewProject", method=RequestMethod.GET)
	public String viewProject(Model model)
	{
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		model.addAttribute("userRole", role);
		return "viewProjectStatus";
	}
	
	@RequestMapping(value="/createVendor", method=RequestMethod.GET)
	public String createVendor(Model model)
	{
		return "createVendor";
	}
	
	@RequestMapping(value="/addVendor", method=RequestMethod.POST)
	public String addVendor(Model model,@RequestParam String name, @RequestParam String contact_number, @RequestParam String notes) 
	{
		List<Vendor> addVendor_entries = dao.findVendor(name);
		
		if(!addVendor_entries.isEmpty()) {
			model.addAttribute("verify", addVendor_entries);
			return "createVendor";
		}
		else {
			Vendor vendors = new Vendor(name, contact_number, notes);
			dao.addVendor(vendors);
			
		    model.addAttribute("addCampus", vendors);
			return "createVendor";
		}	
	}
	
	@RequestMapping(value="/createContractor", method=RequestMethod.GET)
	public String createContractor(Model model)
	{
		return "createContractor";
	}
	
	@RequestMapping(value="/addContractor", method=RequestMethod.POST)
	public String addContractor(Model model, @RequestParam String first_name, @RequestParam String last_name, @RequestParam String email, @RequestParam String contact_number, @RequestParam String notes, @RequestParam String account) 
	{
		List<Contractor> addContractor_entries = dao.findContractor(email);
		
		if(!addContractor_entries.isEmpty()) {
			model.addAttribute("verify", addContractor_entries);
			model.addAttribute("addContractor", true);
			return "createContractor";
		}
		else {
			Contractor contractor = new Contractor(first_name, last_name, email, contact_number, notes);
			
			dao.addContractor(contractor);
			
			if(account.equals("enabled")) {
				String first_character = first_name.substring(0,1);
				
				String username = first_character+last_name;
				String password = first_character+last_name;
				
				String encryptedPassword = new BCryptPasswordEncoder().encode(password);
				User user = new User(username, encryptedPassword, true);
				
				UserRole userRole = new UserRole(user,"ROLE_USER");
				user.getUserRole().add(userRole);
				dao.createUser(user);
			}
		    model.addAttribute("addCampus", contractor);
		    model.addAttribute("addContractor", true);
			return "createContractor";
		}	
	}
	
	@RequestMapping(value="/updatePassword", method=RequestMethod.GET)
	public String updatePassword(Model model)
	{
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		model.addAttribute("userRole", role);
		return "updatePassword";
	}
	
	@RequestMapping(value="/updatePass", method=RequestMethod.GET)
	public String updatePass(Model model)
	{
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		model.addAttribute("userRole", role);
		return "updatePassword";
	}
	
	
	@RequestMapping(value="/updateProject", method=RequestMethod.GET)
	public String updateProject(Model model)
	{
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		model.addAttribute("userRole", role);
		return "updateProject";
	}
	
	@RequestMapping(value="/createBuilding", method=RequestMethod.GET)
	public String createBuilding(Model model)
	{
		return "createBuilding";
	}
	
	@RequestMapping(value="/createProject", method=RequestMethod.GET)
	public String createProject(Model model)
	{
		List<Building> campus = dao.findAllBuilding();
		
		String role_title = "ROLE_ADMIN";
		List<UserRole> projectManager = dao.getProjectManager(role_title);
		System.out.println(projectManager);
		int length = projectManager.size();
		
		ArrayList<String> username = new ArrayList<String>();
		
		for(int i = 0; i < length; i++)
		{
			User user = projectManager.get(i).getUser();
			username.add(user.getUsername());
		}
		
		model.addAttribute("project_managers", username);
		model.addAttribute("campus_building", campus);
		return "createProject";
	}
	
	@RequestMapping(value="/searchWingBasedOnCampus", method=RequestMethod.POST)
	public String searchWingBasedOnCampus(Model model, @RequestParam  String campus_list)
	{
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		
		List<Building> campus = dao.findBuilding(campus_list);
		List<Building> campuses = dao.findNotBuilding(campus_list);
		List<Wing> wings = dao.findWingBasedOnCampus(campus_list);
		
		
		model.addAttribute("campuses_building", campus);
		model.addAttribute("campus_building", campuses);
		model.addAttribute("userRole", role);
		model.addAttribute("wing_list", wings);
		return "createRoom";
	}
	
	@RequestMapping(value="/searchWing", method=RequestMethod.POST)
	public String searchWing(Model model, @RequestParam String campus_list)
	{
		
		List<Building> campus = dao.findBuilding(campus_list);
		
		List<Building> campuses = dao.findNotBuilding(campus_list);
		
		List<Wing> wings = dao.findWingBasedOnCampus(campus_list);
		
		String role_title = "ROLE_ADMIN";
		List<UserRole> projectManager = dao.getProjectManager(role_title);
		System.out.println(projectManager);
		int length = projectManager.size();
		
		ArrayList<String> username = new ArrayList<String>();
		
		for(int i = 0; i < length; i++)
		{
			User user = projectManager.get(i).getUser();
			username.add(user.getUsername());
		}
		
		model.addAttribute("project_managers", username);
		model.addAttribute("campuses_building", campus);
		model.addAttribute("campus_building", campuses);
		model.addAttribute("wing_building", wings);
		model.addAttribute("building_get", "T");
		
		return "createProject";
	}
	
	@RequestMapping(value="/searchRoom", method=RequestMethod.POST)
	public String searchRoom(Model model, @RequestParam String campus_list, @RequestParam String wing_list)
	{
		List<Building> campus = dao.findBuilding(campus_list);
		List<Building> campuses = dao.findNotBuilding(campus_list);
		
		List<Wing> wings = dao.findWing(wing_list, campus_list);
		
		List<Wing> wing = dao.findNotWing(wing_list, campus_list);
		List<Room> rooms = dao.findRoomBasedOnWing(wing_list,campus_list);
		
		System.out.println(rooms);
		
		String role_title = "ROLE_ADMIN";
		List<UserRole> projectManager = dao.getProjectManager(role_title);
		System.out.println(projectManager);
		int length = projectManager.size();
		
		ArrayList<String> username = new ArrayList<String>();
		
		for(int i = 0; i < length; i++)
		{
			User user = projectManager.get(i).getUser();
			username.add(user.getUsername());
		}
		
		model.addAttribute("project_managers", username);
		model.addAttribute("campuses_building", campus);
		model.addAttribute("campus_building", campuses);
		model.addAttribute("wing_buildings", wings);
		model.addAttribute("wing_building", wing);
		model.addAttribute("room_building", rooms);
		
		//Make them not null
		model.addAttribute("building_get", "T");
		model.addAttribute("wing_get", "T");
		
		return "createProject";
	}
	
	@RequestMapping(value="/addProject", method=RequestMethod.POST)
	public String addProject(Model model, @RequestParam String campus_list, @RequestParam String wing_list,
			@RequestParam String room_list,@RequestParam String project_num, @RequestParam String project_name,
			@RequestParam String square_ft, @RequestParam String department, @RequestParam String contact_name, @RequestParam String budget_cost,
			@RequestParam String projected_cost, @RequestParam String adjusted_cost, @RequestParam String percentage,
			@RequestParam String project_phase, @RequestParam String project_manager, @RequestParam String vendor) {
		
		String projectName = project_num + "-" + project_name;
		int project_number = Integer.parseInt(project_num);
		String problem = "No";
		
		List<Project> verifyProject = dao.verifyProject(project_number);
		System.out.println(verifyProject);
		
		if(verifyProject.isEmpty())
		{
			Project project = new Project(campus_list, wing_list, room_list,
					project_number, projectName, square_ft, department, contact_name,
					budget_cost, projected_cost, adjusted_cost, percentage, project_phase, project_manager,problem,vendor);
			
			dao.addProject(project);
			List<Building> campus = dao.findAllBuilding();
			model.addAttribute("campus_building", campus);
			model.addAttribute("verify", true);
			return "createProject";
		}
		else
		{
			model.addAttribute("error",true);
			return "createProject";
		}
		
	}
	
	@RequestMapping(value="/createRoom", method=RequestMethod.GET)
	public String createRoom(Model model)
	{
		List<Building> campus = dao.findAllBuilding();
		List<Wing> wing = dao.findAllWing();
		
		model.addAttribute("campus_building", campus);
		model.addAttribute("wing_list", wing);
		return "createRoom";
	}
	
	
	@RequestMapping(value="/reviewVendor", method=RequestMethod.GET)
	public String reviewVendor(Model model)
	{
		List<Vendor> vendor = dao.getVendors();
		
		model.addAttribute("Vendor",vendor);
		return "reviewVendor";
	}
	
	@RequestMapping(value="/reviewContractor", method=RequestMethod.GET)
	public String reviewContractor(Model model)
	{
		
		String role_title = "ROLE_USER";
		List<UserRole> projectManager = dao.getProjectManager(role_title);
		System.out.println(projectManager);
		int length = projectManager.size();
		
		ArrayList<String> username = new ArrayList<String>();
		
		for(int i = 0; i < length; i++)
		{
			User user = projectManager.get(i).getUser();
			username.add(user.getUsername());
		}
		
		model.addAttribute("Contractor",username);
		return "reviewContractor";
	}
	
	@RequestMapping(value="/createWing", method=RequestMethod.GET)
	public String createWing(Model model)
	{
		
		List<Building> campuses = dao.findAllBuilding();

		model.addAttribute("campus_building", campuses);
		return "createWing";
	}
	
	@RequestMapping(value="/contact", method=RequestMethod.GET)
	public String contact(Model model)
	{
		return "contactAdmin";
	}
	
	@RequestMapping(value="/adminAccess", method=RequestMethod.GET)
	public String adminAccess(Model model)
	{
		return "adminAccess";
	}
	
	@RequestMapping(value="/searchProject", method=RequestMethod.GET)
	public String searchProject(Model model)
	{
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		
		
		List<Project> projects = dao.getProjects();
		
		model.addAttribute("toogle","True");
		model.addAttribute("userName", userName);
		model.addAttribute("userRole", role);
		model.addAttribute("Project", projects);
		
		return "searchProject";
	}
	
	@RequestMapping(value="/searchProjectProblem", method=RequestMethod.GET)
	public String searchProjectProblem(Model model)
	{
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		
		List<Project> projects = dao.getProjectProblem();
		model.addAttribute("Problem", "No");
		model.addAttribute("userRole", role);
		model.addAttribute("Project", projects);
		return "searchProject";
	}
	
	@RequestMapping(value="/viewProblem/{projectName}", method=RequestMethod.GET)
	public String viewProblems(Model model, @PathVariable String projectName)
	{
		
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
	
		
		List<Problem> problem = dao.getProblem(projectName);
		model.addAttribute("Problem", problem);
		model.addAttribute("userRole", role);
		return "viewProblem";
	}
	@RequestMapping(value="/searchProjectProblems", method=RequestMethod.GET)
	public String searchProjectProblems(Model model)
	{
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		
		List<Project> projects = dao.getProjectProblems();
		model.addAttribute("Problem", "Yes");
		model.addAttribute("userRole", role);
		model.addAttribute("Project", projects);
		return "searchProject";
	}
	
	@RequestMapping(value="/viewProjects/{project_num}", method=RequestMethod.GET)
	public String viewProject(Model model, @PathVariable int project_num) 
	{	
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		model.addAttribute("userRole", role);
		
		List<Project> projectList = dao.getProjectFromProjectNum(project_num);
		
		model.addAttribute("Project", projectList);
		return "viewProjectDetails";
	}
	

	@RequestMapping(value="/updateProject/{project_id}", method=RequestMethod.GET)
	public String updateProject(Model model, @PathVariable int project_id) 
	{		
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		model.addAttribute("userRole", role);
		
		List<Project> projectList = dao.getProjectFromId(project_id);
		
		String role_title = "ROLE_ADMIN";
		List<UserRole> projectManager = dao.getProjectManager(role_title);
		System.out.println(projectManager);
		int length = projectManager.size();
		
		ArrayList<String> username = new ArrayList<String>();
		
		for(int i = 0; i < length; i++)
		{
			User user = projectManager.get(i).getUser();
			username.add(user.getUsername());
		}
		
		List<Vendor> vendor = dao.getVendors();
		
		model.addAttribute("Project", projectList);
		model.addAttribute("project_manager", username);
		model.addAttribute("Vendor",vendor);
		return "updateProject";
	}
	
	@RequestMapping(value="/updateNewProject/{project_id}", method=RequestMethod.POST)
	public String updateNewProject(Model model, @RequestParam String campus_list, @RequestParam String wing_list,
			@RequestParam String room_list, @RequestParam String project_num, @RequestParam String project_name,
			@RequestParam String square_ft, @RequestParam String department, @RequestParam String contact_name, 
			@RequestParam String budget_cost, @RequestParam String projected_cost, 
			@RequestParam String adjusted_cost, @PathVariable int project_id, @RequestParam String percentage,
			@RequestParam String project_phase, @RequestParam String project_manager, 
			@RequestParam String problem, @RequestParam String vendor) {
		
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		model.addAttribute("userRole", role);
		
		if(problem.equalsIgnoreCase("enabled"))
		{
			int project_number = Integer.parseInt(project_num);
			//double square_feet = Double.parseDouble(square_ft);
			String problems = "No";
			Project project = new Project(campus_list, wing_list, room_list,
					project_number, project_name, square_ft, department, contact_name,
					budget_cost, projected_cost, adjusted_cost, percentage, project_phase, project_manager,problems,vendor);
			
			dao.addProject(project);
			
			dao.deleteProject(project_id);
			
			model.addAttribute("project_name", project_name);
			model.addAttribute("username", userName);
			
			return "problem";
		}
		else
		{
			int project_number = Integer.parseInt(project_num);
			//double square_feet = Double.parseDouble(square_ft);
			String problems = "No";
			
			Project project = new Project(campus_list, wing_list, room_list,
					project_number, project_name, square_ft, department, contact_name,
					budget_cost, projected_cost, adjusted_cost, percentage, project_phase, project_manager,problems,vendor);
			
			dao.addProject(project);
			
			dao.deleteProject(project_id);
			
			model.addAttribute("Submitted", true);
			return "searchProject";
		}
	}
	
	@RequestMapping(value="/addProblem", method=RequestMethod.POST)
	public String addProblem(Model model, @RequestParam String description, @RequestParam String project_name,
			@RequestParam String username) {
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		System.out.println(userName);
		model.addAttribute("userRole", role);
		
		Problem problem = new Problem(project_name,username,description);
		
		dao.addProblem(problem, project_name);
		
		model.addAttribute("add_problem", true);
		return "searchProject";
	}
		
	@RequestMapping(value="/closeProject/{project_id}", method=RequestMethod.GET)
	public String closeProject(Model model, @PathVariable int project_id)
	{
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		
		dao.closeProject(project_id);
		
		List<Project> projectList = dao.getProjects();
		
		model.addAttribute("close",true);
		model.addAttribute("userRole", role);
		model.addAttribute("Project", projectList);
		return "searchProject";
	}
	
	@RequestMapping(value="/getAllProject", method=RequestMethod.GET)
	public String getAllProject(Model model) {
		
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		
		List<Project> projectList = dao.getAllProject();
		
		model.addAttribute("userName", userName);
		model.addAttribute("userRole", role);
		model.addAttribute("Project", projectList);
		return "searchProject";
	}
	
	@RequestMapping(value="/viewContractorDetail/{project_id}", method=RequestMethod.GET)
	public String getContractDetail(Model model, @PathVariable String project_id) {

		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		
		List<ContractorDetails> detailList = dao.getContractorDetail(project_id);
		
		model.addAttribute("userRole", role);
		model.addAttribute("detailList", detailList);
		return "viewContractorDetail";
	}
	
	@RequestMapping(value="/addContractorDetail/{project_id}", method=RequestMethod.GET)
	public String addContractDetail(Model model, @PathVariable int project_id) {
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		
		List<Project> project = dao.getMonitaryBasedOnProjectId(project_id);
		
		String role_title = "ROLE_USER";
		List<UserRole> projectManager = dao.getProjectManager(role_title);
		System.out.println(projectManager);
		int length = projectManager.size();
		
		ArrayList<String> contractor = new ArrayList<String>();
		
		for(int i = 0; i < length; i++)
		{
			User user = projectManager.get(i).getUser();
			contractor.add(user.getUsername());
		}
		
		List<Vendor> vendor = dao.getVendors();
		
		model.addAttribute("contractor",contractor);
		model.addAttribute("vendor", vendor);
		model.addAttribute("userRole", role);
		model.addAttribute("Project",project);
		return "addContractorDetail";
	}
	
	@RequestMapping(value="/addContractorDetail", method=RequestMethod.POST)
	public String addContractorDetail(Model model, @RequestParam String project_id,
			@RequestParam String contractor_vendor, @RequestParam String actual_cost, @RequestParam String estimate_cost,
			@RequestParam String work_description, @RequestParam String work_note) {
		
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		
		int id = Integer.parseInt(project_id);
		
		ContractorDetails detail = new ContractorDetails(project_id,contractor_vendor, work_description,
				actual_cost, estimate_cost,work_note);
		
		dao.addContractorDetails(detail, actual_cost, estimate_cost, id);
		

		List<Project> projects = dao.getProjects();
		
		
		model.addAttribute("details_added",true);
		model.addAttribute("userRole", role);
		model.addAttribute("Project", projects);
		return "searchProject";
	}
	
	@RequestMapping(value="/removeContractor/{project_num}", method=RequestMethod.GET)
	public String removeContractor(Model model, @PathVariable int project_num) {
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		
		dao.removeContractor(project_num);
		
		List<Project> projectList = dao.getProjectFromProjectNum(project_num);
		
		model.addAttribute("Project", projectList);
		model.addAttribute("userRole", role);
		return "viewProjectDetails";
		
	}
	@RequestMapping(value="/removeVendor/{project_num}", method=RequestMethod.GET)
	public String removeVendor(Model model, @PathVariable int project_num) {
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		
		dao.removeVendor(project_num);
		
		List<Project> projectList = dao.getProjectFromProjectNum(project_num);
		
		model.addAttribute("Project", projectList);
		model.addAttribute("userRole", role);
		return "viewProjectDetails";
		
	}
	
	@RequestMapping(value="/submitContractorReview", method=RequestMethod.POST)
	public String submitContractorReview(Model model, @RequestParam String contractor,
			@RequestParam String description) {
		
		ContractorReview contractors = new ContractorReview(contractor,description);
		
		dao.addContractorReview(contractors);
		
		model.addAttribute("Submitted", true);
		return "reviewContractor";
	}
	@RequestMapping(value="/submitVendorReview", method=RequestMethod.POST)
	public String submitVendorReview(Model model, @RequestParam String vendor,
			@RequestParam String description) {
		
		VendorReview vendors = new VendorReview(vendor,description);
		
		dao.addVendorReview(vendors);
		
		model.addAttribute("Submitted", true);
		return "reviewContractor";
	}
	
	@RequestMapping(value="/viewVendorContractorReview", method=RequestMethod.POST)
	public String viewVendorReview(Model model, @RequestParam String name) {
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		
		List<VendorReview> vendorReview = dao.getVendorReview(name);
		
		System.out.println(vendorReview);
		
		List<ContractorReview> contractorReview = dao.getContractor(name);
		
		System.out.println(contractorReview);
		
		model.addAttribute("userRole", role);
		model.addAttribute("vendor", vendorReview);
		model.addAttribute("contractor", contractorReview);
		return "seeReview";
	}
	
	@RequestMapping(value="/viewReview", method=RequestMethod.GET)
	public String viewReview(Model model) {
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		
		String role_title = "ROLE_USER";
		List<UserRole> projectManager = dao.getProjectManager(role_title);
		System.out.println(projectManager);
		int length = projectManager.size();
		
		ArrayList<String> contractor = new ArrayList<String>();
		
		for(int i = 0; i < length; i++)
		{
			User user = projectManager.get(i).getUser();
			contractor.add(user.getUsername());
		}
		
		List<Vendor> vendor = dao.getVendors();
		
		model.addAttribute("contractor", contractor);
		model.addAttribute("vendor",vendor);
		model.addAttribute("userRole", role);
		return "viewReview";
	}
	@RequestMapping(value="/contactRequest", method=RequestMethod.GET)
	public String submitIssue(Model model) {
		
		Collection<SimpleGrantedAuthority> userRole = (Collection<SimpleGrantedAuthority>) 
				SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		String role = userRole.toString();
		
		model.addAttribute("userRole", role);
		model.addAttribute("addIssue", true);
		return "contactAdmin";
	}
}