package ca.sheridancollege.dao;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import ca.sheridancollege.beans.Building;
import ca.sheridancollege.beans.Contractor;
import ca.sheridancollege.beans.ContractorDetails;
import ca.sheridancollege.beans.ContractorReview;
import ca.sheridancollege.beans.Problem;
import ca.sheridancollege.beans.Project;
import ca.sheridancollege.beans.Room;
import ca.sheridancollege.beans.User;
import ca.sheridancollege.beans.UserRole;
import ca.sheridancollege.beans.Vendor;
import ca.sheridancollege.beans.VendorReview;
import ca.sheridancollege.beans.Wing;

public class DAO {
	SessionFactory sessionFactory = new Configuration()
			.configure("ca/sheridancollege/config/hibernate.cfg.xml")
			.buildSessionFactory();

	public User findByUserName(String username) {
		return (User) sessionFactory.openSession()
		.createNamedQuery("User.getUserByUsername")
		.setParameter("username", username)
		.getSingleResult();
	}
	
	public List<Wing> findWings(String campus){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		ProjectionList projList = Projections.projectionList();
		projList.add(Projections.property("wings_name"));
		Criteria crit = session.createCriteria(Wing.class)
				.add(Restrictions.eq("campuses",campus));
				
		List wings = crit.list();
				
		session.getTransaction().commit();
		session.close();
		
		return wings;
	}
	
	
	public List<Building> findAllBuilding() {
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			
			
			Criteria crit = session.createCriteria(Building.class);
		    ProjectionList projList = Projections.projectionList();
		    projList.add(Projections.property("building_name"));
		    crit.setProjection(projList);
		    
		    List campuses = crit.list();
			return campuses;
			
	}
	
	public List<Wing> findAllWing() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Criteria crit = session.createCriteria(Wing.class);
	    ProjectionList projList = Projections.projectionList();
	    projList.add(Projections.property("wings_name"));
	    crit.setProjection(projList);
	    
	    List wings = crit.list();
		return wings;
}
	
	
	public void createUser(User user) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(user);
		
		session.getTransaction().commit();
		session.close();
	}
	
	public List<Building>findBuilding(String building){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		ProjectionList projList = Projections.projectionList();
		projList.add(Projections.property("building_name"));
		Criteria crit = session.createCriteria(Building.class)
				.add(Restrictions.eq("building_name",building))
				.setProjection(projList);
		
		List buildings = crit.list();
		
		session.getTransaction().commit();
		session.close();
		
		return buildings;
	}
	
	public List<Building>findNotBuilding(String building){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		ProjectionList projList = Projections.projectionList();
		projList.add(Projections.property("building_name"));
		Criteria crit = session.createCriteria(Building.class)
				.add(Restrictions.ne("building_name",building))
				.setProjection(projList);
		
		List buildings = crit.list();
		
		session.getTransaction().commit();
		session.close();
		
		return buildings;
	}
	
	public List<Wing>findWing(String wing, String campus){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		ProjectionList projList = Projections.projectionList();
		projList.add(Projections.property("wings_name"));
		Criteria crit = session.createCriteria(Wing.class)
				.add(Restrictions.eq("wings_name",wing))
				.add(Restrictions.eq("campuses",campus))
				.setProjection(projList);
		
		List wings = crit.list();
		
		session.getTransaction().commit();
		session.close();
		
		return wings;
	}
	
	public List<Wing>findNotWing(String wing, String campus){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		ProjectionList projList = Projections.projectionList();
		projList.add(Projections.property("wings_name"));
		Criteria crit = session.createCriteria(Wing.class)
				.add(Restrictions.eq("campuses",campus))
				.add(Restrictions.ne("wings_name",wing))
				.setProjection(projList);
		
		List wings = crit.list();
		
		session.getTransaction().commit();
		session.close();
		
		return wings;
	}
	
	public List<Wing>findInfo(String campus, String wing){
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<Wing> criteria = criteriaBuilder.createQuery(Wing.class);

		Root<Wing> root = criteria.from(Wing.class);

		criteria.where(
				criteriaBuilder.and(
					criteriaBuilder.equal(root.get("campuses"),campus), 
					criteriaBuilder.equal(root.get("wings_name"),wing)
					)
				);
		
		List<Wing> wingList = session.createQuery(criteria).getResultList();

		session.getTransaction().commit();
		session.close();
		
		return wingList;
	}
	
	public List<Wing>findWingBasedOnCampus(String campus)
	{
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		ProjectionList projList = Projections.projectionList();
		projList.add(Projections.property("wings_name"));
		Criteria crit = session.createCriteria(Wing.class)
				.add(Restrictions.eq("campuses",campus))
				.setProjection(projList);
		
		List wings = crit.list();
		
		session.getTransaction().commit();
		session.close();
		
		return wings;
	}
	
	public List<Room>findRoomBasedOnWing(String wing,String campus)
	{
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		ProjectionList projList = Projections.projectionList();
		projList.add(Projections.property("room_name"));
		Criteria crit = session.createCriteria(Room.class)
				.add(Restrictions.eq("wing",wing))
				.add(Restrictions.eq("locations",campus))
				.setProjection(projList);
		
		List room = crit.list();
		
		session.getTransaction().commit();
		session.close();
		
		return room;
	}
	
	public List<Room>findRoom(String campus, String wing, String room_name){
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<Room> criteria = criteriaBuilder.createQuery(Room.class);

		Root<Room> root = criteria.from(Room.class);

		criteria.where(
				criteriaBuilder.and(
					criteriaBuilder.equal(root.get("locations"),campus), 
					criteriaBuilder.equal(root.get("wing"),wing),
					criteriaBuilder.equal(root.get("room_name"),room_name)
					)
				);
		
		List<Room> room = session.createQuery(criteria).getResultList();

		session.getTransaction().commit();
		session.close();
		
		return room;
	}
	
	//public void updatePassword(User user) {
		
	//}
	
	
	public void addBuilding(Building campus) {
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(campus);
		
		session.getTransaction().commit();
		session.close();
		
	}
	
	public void addProject(Project project) {
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(project);
		
		session.getTransaction().commit();
		session.close();
		
	}
	

	public void addRooms(Room room) {
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(room);
		
		session.getTransaction().commit();
		session.close();
		
	}
	
	public void addWing(Wing wing) {
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(wing);
		
		session.getTransaction().commit();
		session.close();
	}
	
	public List<Project> getProjects(){
		Session session = sessionFactory.openSession();

		session.beginTransaction();
		
		Query query = session.getNamedQuery("Project.getProjects");
		
		List<Project> projectList = query.getResultList();
		
		session.getTransaction().commit();
		session.close();
		
		return projectList;
	}
	
	public List<Project> getAllProject(){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query = session.getNamedQuery("Project.getAllProject");
		
		List<Project> projectList = query.getResultList();
		
		session.getTransaction().commit();
		session.close();
		
		return projectList;
	}
	
	public List<Project> getProjectProblem(){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query = session.getNamedQuery("Project.getAllProjectProblem");
		
		List<Project> projectList = query.getResultList();
		
		session.getTransaction().commit();
		session.close();
		
		return projectList;	
	}
	
	public List<Project> getProjectProblems(){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query = session.getNamedQuery("Project.getAllProjectProblems");
		
		List<Project> projectList = query.getResultList();
		
		session.getTransaction().commit();
		session.close();
		
		return projectList;	
	}
	
	public List<Project> getProjectFromProjectNum(int projectNum)
	{
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		
		Query query= session.createNamedQuery("Project.getProjectByProjectNum");
		query.setParameter("project_num", projectNum);
		
		
		List<Project> projectList = query.getResultList();
		
		session.getTransaction().commit();
		session.close();
		
		return projectList;
	}
	
	public List<Problem> getProblem(String projectName)
	{
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query= session.createNamedQuery("Problem.getProjectByProjectName");
		query.setParameter("projectName", projectName);
		
		List<Problem> problem = query.getResultList();
		
		session.getTransaction().commit();
		session.close();
		
		return problem;
	}
	
	public List<Project> getProjectFromId(int id)
	{
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		
		Query query= session.createNamedQuery("Project.getProjectById");
		query.setParameter("id", id);
		
		
		List<Project> projectList = query.getResultList();
		
		session.getTransaction().commit();
		session.close();
		
		return projectList;
	}
	
	public void deleteProject(int id)
	{
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Project toDelete = session.get(Project.class, id);
		session.delete(toDelete);
		
		session.getTransaction().commit();
		session.close();	
	}
	
	public List<Vendor>findVendor(String name){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		ProjectionList projList = Projections.projectionList();
		projList.add(Projections.property("name"));
		Criteria crit = session.createCriteria(Vendor.class)
				.add(Restrictions.eq("name",name))
				.setProjection(projList);
		
		List vendors = crit.list();
		
		session.getTransaction().commit();
		session.close();
		
		return vendors;
	}
	
	public void addVendor(Vendor vendor) {
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(vendor);
		
		session.getTransaction().commit();
		session.close();
		
	}
	
	public List<Contractor>findContractor(String email){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		ProjectionList projList = Projections.projectionList();
		projList.add(Projections.property("email"));
		Criteria crit = session.createCriteria(Contractor.class)
				.add(Restrictions.eq("email",email))
				.setProjection(projList);
		
		List contractor = crit.list();
		
		session.getTransaction().commit();
		session.close();
		
		return contractor;
	}
	
	public void addContractor(Contractor contractor) {
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(contractor);
		
		session.getTransaction().commit();
		session.close();
		
	}
	
	public void closeProject(int project_id) {
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query = session.createQuery("UPDATE Project SET project_phase =:project_phase, percentage =:percentage WHERE id =:id");
		query.setParameter("project_phase", "Closed");
		query.setParameter("percentage", "100");
		query.setParameter("id", project_id);
		query.executeUpdate();
		
		session.getTransaction().commit();
		session.close();
	}
	
	public List<UserRole> getProjectManager(String role) {
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query = session.getNamedQuery("GetManager");
		query.setParameter("role", role);
		
		List<UserRole> project_manager = query.getResultList();
		
		session.getTransaction().commit();
		session.close();
	   
		return project_manager;
	}
	
	public List<ContractorReview> getContractor(String contractor){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query = session.getNamedQuery("GetContractor");
		query.setParameter("contractor", contractor);
		
		List<ContractorReview> contractorReview = query.getResultList();
		
		session.getTransaction().commit();
		session.close();
	   
		return contractorReview;
	}
	
	public List<VendorReview> getVendorReview(String vendor){
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query = session.getNamedQuery("GetVendorsReview");
		query.setParameter("vendor", vendor);
		
		List<VendorReview> vendorReview = query.getResultList();
		
		session.getTransaction().commit();
		session.close();
	   
		return vendorReview;
	}
	
	public List<Vendor> getVendors(){
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		ProjectionList projList = Projections.projectionList();
		projList.add(Projections.property("name"));
		Criteria crit = session.createCriteria(Vendor.class)
				.setProjection(projList);
		
		List vendor = crit.list();
		
		session.getTransaction().commit();
		session.close();
	   
		return vendor;
	}
	
	public void addProblem(Problem problem, String project_name) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(problem);
		
		Query query = session.createQuery("UPDATE Project SET problem =:problem WHERE projectName =:projectName");
		query.setParameter("problem", "Yes");
		query.setParameter("projectName", project_name);
		query.executeUpdate();
		
		session.getTransaction().commit();
		session.close();	
	}
	
	public void updatePassword(String username, String password) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query = session.createQuery("UPDATE User SET password =:password WHERE username =:username");
		query.setParameter("username", username);
		query.setParameter("password", password);
		query.executeUpdate();
		
		session.getTransaction().commit();
		session.close();
		
	}
	
	public List<Project>getMonitaryBasedOnProjectId(int project_id){
		Session session = sessionFactory.openSession();
		session.beginTransaction(); 
		

		Query query = session.getNamedQuery("Project.getProjectById");
		query.setParameter("id", project_id);
		
		List<Project> projectDetails = query.getResultList();
		
		session.getTransaction().commit();
		session.close();
		
		return projectDetails;
	}
	
	public List<ContractorDetails>getContractorDetail(String project_id)
	{
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query = session.getNamedQuery("Contractor.getDetailsProjectName");
		query.setParameter("project_id", project_id);
		
		List<ContractorDetails> contractorDetails = query.getResultList();
		
		session.getTransaction().commit();
		session.close();
		
		return contractorDetails;
	}
	
	public void addContractorDetails(ContractorDetails details, String actual_cost, String estimate_cost, int id)
	{
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(details);
		
		Query query = session.createQuery("UPDATE Project SET actual_cost =:actual_cost, projected_cost =:projected_cost WHERE id =:id");
		query.setParameter("actual_cost", actual_cost);
		query.setParameter("projected_cost", estimate_cost);
		query.setParameter("id", id);
		query.executeUpdate();
		
		session.getTransaction().commit();
		session.close();
	}
	
	public List<Project> verifyProject(int project_num)
	{
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query = session.getNamedQuery("verifyProject");
		query.setParameter("project_num", project_num);
		
		List<Project> verifyProject = query.getResultList();
		
		session.getTransaction().commit();
		session.close();
	   
		return verifyProject;
	}
	
	public void removeContractor(int project_num)
	{
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query = session.createQuery("UPDATE Project SET project_manager =:project_manager WHERE project_num =:project_num");
		query.setParameter("project_manager", "N/A");
		query.setParameter("project_num", project_num);
		query.executeUpdate();
		
		session.getTransaction().commit();
		session.close();  
	}
	
	public void removeVendor(int project_num)
	{
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Query query = session.createQuery("UPDATE Project SET vendor =:vendor WHERE project_num =:project_num");
		query.setParameter("vendor", "N/A");
		query.setParameter("project_num", project_num);
		query.executeUpdate();
		
		session.getTransaction().commit();
		session.close();  
	}
	
	public void addVendorReview(VendorReview vendor)
	{
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(vendor);
		
		session.getTransaction().commit();
		session.close();
	}
	
	public void addContractorReview(ContractorReview contractor)
	{
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(contractor);
		
		session.getTransaction().commit();
		session.close();
	}
}
